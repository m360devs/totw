<?php
// Setup
define( 'M360_DEV_MODE', true);

//Includes
include( get_theme_file_path( '/includes/enqueue.php' ) );
include( get_theme_file_path( '/includes/setup.php' ) );
include( get_theme_file_path( '/includes/admin.php' ) );
include( get_theme_file_path( '/includes/plugins/acf.php' ) );
include( get_theme_file_path( '/includes/plugins/cf7.php' ) );

//Hooks
add_action( 'wp_enqueue_scripts', 'm360_enqueue' );
add_action( 'after_setup_theme', 'm360_setup_theme' );
add_action( 'login_enqueue_scripts', 'my_login_logo' );

//Shortcodes

//Plugin config
add_action( 'after_setup_theme', 'acfConfig' );
add_action( 'after_setup_theme', 'cf7Config' );

// Disable options
include( get_theme_file_path('/includes/disabled.php') );