<!-- ACF Page builder -->

<?php
if (have_posts()) : while (have_posts()) : the_post();

  if( have_rows('page_builder') ): 

    while ( have_rows('page_builder') ) : the_row();

      // Hero
      if( get_row_layout() == 'hero_carousel')
      get_template_part('src/components/builder-components/carousel/hero-carousel');

      //Image text to left
      if( get_row_layout() == 'text_image')
      get_template_part('src/components/builder-components/text-image/text-image');

      //Image text to right
      if( get_row_layout() == 'image_text')
      get_template_part('src/components/builder-components/image-text/image-text');

      //Full width text
      if( get_row_layout() == 'full_width_text')
      get_template_part('src/components/builder-components/full-width-text/full-width-text');

      //Full width
      if( get_row_layout() == 'full_width')
      get_template_part('src/components/builder-components/full-width/full-width');

      //Text hero
      if( get_row_layout() == 'text_hero')
      get_template_part('src/components/builder-components/text-hero/text-hero');

      //Services section
      if( get_row_layout() == 'services_section')
      get_template_part('src/components/builder-components/services/services');

      //Services with icons
      if( get_row_layout() == 'service_icons')
      get_template_part('src/components/builder-components/services-icons/services-icons');

      //Contact
      if( get_row_layout() == 'contact')
      get_template_part('src/components/builder-components/contact/contact');

      //Contact Global
      if( get_row_layout() == 'global_contact')
      get_template_part('src/components/builder-components/contact-global/contact-global');

      //Full width image
      if( get_row_layout() == 'full_width_image')
      get_template_part('src/components/builder-components/full-width-image/full-width-image');

      //Pet food
      if( get_row_layout() == 'pet_food')
      get_template_part('src/components/builder-components/pet-food/pet-food');

      //Projects
      if( get_row_layout() == 'projects')
      get_template_part('src/components/builder-components/projects/projects');

      //Project with carousel
      if( get_row_layout() == 'project')
      get_template_part('src/components/builder-components/project/project');

      //Project with carousel
      if( get_row_layout() == 'products')
      get_template_part('src/components/builder-components/products/products');

    endwhile; 
  endif; 
endwhile; 
endif; 
