<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>

    <meta charset="<?php bloginfo('charset')?>" >
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php wp_head(); ?>
    
    <?php // favicons generated at https://realfavicongenerator.net/ ?>

    <link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/m360theme/files/icons/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/m360theme/files/icons/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/m360theme/files/icons/favicons/favicon-16x16.png">
    <link rel="manifest" href="/wp-content/themes/m360theme/files/icons/favicons/site.webmanifest">
    <link rel="mask-icon" href="/wp-content/themes/m360theme/files/icons/favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/wp-content/themes/m360theme/files/icons/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="/wp-content/themes/m360theme/files/icons/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <?php // make directory URL available to scripts ?>

    <script type="text/javascript" defer>
      var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>";
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-ECX0MGEGVL"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-ECX0MGEGVL');
    </script>
  </head>

  <?php get_template_part('src/components/regions/header/header'); ?>

  <body <?php body_class(); ?>>