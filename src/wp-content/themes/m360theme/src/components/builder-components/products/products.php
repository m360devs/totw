<!-- Products-->
<?php $rows = get_sub_field('products_repeater');
$padding = get_sub_field('products_padding');?>

<section class="products <?php echo $padding; ?>">
  <div class="container-fluid products__container content-container no-padding">
    <div class="row products__row">

      <?php
      if( $rows ) {
      foreach( $rows as $row ) {
      $title = $row['products_heading'];
      $excerpt = $row['products_excerpt'];
      $link = $row['products_link'];
      $image = $row['products_image']; ?>

      <div class="col-sm-4">
        <div class="products__outer">
          <div class="products__image">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo ($image['alt'] ? $image['alt'] : ''); ?>" class="products__icon">
          </div>
          <div class="products__content">
          <div class="products__content-top">
            <h4 class="products__title"><?php echo $title; ?></h4>
            <p class="products__row"><?php echo $excerpt; ?></p>
          </div>
            <a href="<?php echo $link['url']; ?>" class="products__button"><?php echo $link['title']; ?></a>
          </div>
        </div>
      </div>
      <?php 
        }
      }?>
    </div>
  </div>
</section>





