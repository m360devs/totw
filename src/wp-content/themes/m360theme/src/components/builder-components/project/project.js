//##########################################################################
// Project with slides js
//##########################################################################

export const project = {
  elements: {
    header: null,
    sync1: null,
    sync2: null,
    slidesPerPage: null,
    slidesPerPageTablet: null,
    slidesPerPageMobile: null,
    syncedSecondary: null,
  },

  state: {
    menuOpen: false,
  },

  init: function () {
    var self = this;
    self.elements.sync1 = $("#sync1");
    self.elements.sync2 = $("#sync2");
    self.elements.slidesPerPage = 3; //globaly define number of elements per page
    self.elements.slidesPerPageTablet = 3;
    self.slidesPerPageMobile = 3;
    self.syncedSecondary = true;

    //owl carousel with thumbnail nav for gallery template
    if ($(".gallery")) {
      self.elements.sync1
        .owlCarousel({
          items: 1,
          slideSpeed: 2000,
          nav: true,
          navText: [
            "<span><img src='" +
              stylesheet_directory_uri +
              "/files/img/icons/left-chevron.svg'></span>",
            "<span class='min-w-55'><img src='" +
              stylesheet_directory_uri +
              "/files/img/icons/right-chevron.svg'></span>",
          ],
          autoplay: false,
          dots: false,
          // loop: false,
          responsiveRefreshRate: 200,
        })
        .on("changed.owl.carousel", syncPosition);

      self.elements.sync2
        .on("initialized.owl.carousel", function () {
          self.elements.sync2.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
          responsive: {
            0: {
              items: self.elements.slidesPerPageMobile,
            },
            480: {
              items: self.elements.slidesPerPageTablet,
            },
            768: {
              items: self.elements.slidesPerPage,
            },
          },
          dots: false,
          nav: true,
          // stagePadding: 50,

          navText: [
            "<span><img src='" +
              stylesheet_directory_uri +
              "/files/img/icons/left-chevron.svg'></span>",
            "<span class='min-w-55'><img src='" +
              stylesheet_directory_uri +
              "/files/img/icons/right-chevron.svg'></span>",
          ],
          smartSpeed: 200,
          slideSpeed: 500,
          slideBy: self.elements.slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
          responsiveRefreshRate: 100,
        })
        .on("changed.owl.carousel", syncPosition2);

      function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        // var current = el.item.index;

        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

        if (current < 0) {
          current = count;
        }
        if (current > count) {
          current = 0;
        }

        //end block

        self.elements.sync2
          .find(".owl-item")
          .removeClass("current")
          .eq(current)
          .addClass("current");
        var onscreen = self.elements.sync2.find(".owl-item.active").length - 1;
        var start = self.elements.sync2
          .find(".owl-item.active")
          .first()
          .index();
        var end = self.elements.sync2.find(".owl-item.active").last().index();

        if (current > end) {
          self.elements.sync2.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
          self.elements.sync2
            .data("owl.carousel")
            .to(current - onscreen, 100, true);
        }
      }

      function syncPosition2(el) {
        if (self.elements.syncedSecondary) {
          var number = el.item.index;
          self.elements.sync1.data("owl.carousel").to(number, 100, true);
        }
      }

      self.elements.sync2.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        self.elements.sync1.data("owl.carousel").to(number, 300, true);
      });
    }
  },
};

document.addEventListener("DOMContentLoaded", () => {
  project.init();
});
