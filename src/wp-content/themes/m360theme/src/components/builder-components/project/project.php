<!-- Project with carousel -->

<?php 
// echo '<pre>';
// print_r(get_fields());
// echo '</pre>';
?>

<section class="project section--padding-top">
  <div class="container content-container">
    <div class="row">
      <div class="col-lg-6">
        <div class="project__content-container">
          <div class="project__content">
            <h2 class="project__title"><?php echo(get_sub_field('project_title'));?></h2>
            <div class="project__body">
              <?php echo(get_sub_field('project_body'));?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6">

        <div id="sync1" class="project__carousel project__project-carousel owl-carousel owl-theme">
          <?php $rows = get_sub_field('project_images'); 
          if( $rows ) {
          foreach( $rows as $row ) { ?>
          <div class="project__slide">
          <img srcset="<?php echo $row['project_image']['sizes']['527*321']?> 1x, <?php echo $row['project_image']['sizes']['1054*642']?> 2x" src="small.jpg" alt="<?php echo $slide['project_image']['alt']?> " class="img-fluid"  />

          </div>
          <?php } }?>
        </div>

          <div id="sync2" class="project__carousel project__nav-carousel  owl-carousel owl-theme">
          <?php if( $rows ) {
          foreach( $rows as $row ) { ?>
            <div class="project__nav-slide">
            <img src="<?php echo $row['project_image']['sizes']['527*321']?>" alt="<?php echo $slide['project_image']['alt']?> " class="img-fluid"  />
            </div>
            <?php } }?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>





