<!-- Contact form -->

<?php $fields = get_field('global_fields_group', 'options');?>
<section class="contact section--padding section--padding-large">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-6">
        <h2 class="contact__heading"><?php echo(get_sub_field('contact_heading'));?></h2>
        <?php if(get_sub_field('contact_info'));{ ?>
        <div class="contact__info">
            <?php echo(get_sub_field('contact_info'));?>
        </div>
        <?php } ?>
        <?php echo do_shortcode( '[contact-form-7 id="64" title="Main Contact Form" html_class="contact-form"]' ); ?>
      </div>

      <div class="offset-lg-2 col-lg-4 contact__outer">
        <div class="contact__content">

          <div class="row">
            <div class="col-sm-6 col-lg-12">
              <div class="contact__top">
                <span>Telephone: <a href="<?php echo $fields['global_telephone']['url']; ?>" class="bold"><?php echo $fields['global_telephone']['title']; ?></a></span>
                <span>Email: <a href="<?php echo $fields['global_email']['url']; ?>" class="bold"><?php echo $fields['global_email']['title']; ?></a></span>
              </div>
              <div class="contact__mid">
                <?php echo $fields['global_address']; ?>
              </div>
            </div>
            <div class="col-sm-6 col-lg-12">

            <?php if(get_sub_field('contact_map')){ ?>
              <div class="acf-map contact__map" data-zoom="16">
                  <div class="marker" data-lat="<?php echo esc_attr(get_sub_field('contact_map')['lat']); ?>" data-lng="<?php echo (get_sub_field('contact_map')['lng']); ?>">
                  
                  </div>
                  
              </div>
            <?php }?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>