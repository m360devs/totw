<!-- Simple hero with colour background -->

<?php $row = get_sub_field('text_hero'); 
$title = $row['text_hero_heading'] ? $row['text_hero_heading']:"" ;
$BGColor = "background-color:" . $row['text_hero_colour'];
$BGImage = "background-image:url(" . $row['text_hero_image']['url'] . ")";
$textColor ="color:" . $row['text_hero_text_colour']; 
$id = str_replace(" ", "-", strtolower($title));
?>

<section id="<?php echo $id; ?>" class="int-header" style="<?php echo $BGColor; ?>; 
                                  <?php echo $textColor; ?>; 
                                  <?php echo $BGImage; ?>;
                                  ">
  <div class="int-header__content">
    <h1 class="int-header__title"><?php echo $title; ?></h1>
  </div>
</section>