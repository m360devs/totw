<!-- Projects-->

<section class="projects <?php echo get_sub_field('projects_padding'); ?>">
<h2 class="projects__title">Latest Projects</h2>
  <div class="container-fluid">
    <div class="row">
    <div class="col-md-6 col-lg-4 projects__content">
      <a href="#">
        <img src="<?php echo theme_uri() ?>/files/img/projects/1.png" alt="">
      </a>
    </div>
    <div class="col-md-6 col-lg-4 projects__content">
      <a href="#">
       <img src="<?php echo theme_uri() ?>/files/img/projects/2.png" alt="">
      </a>
    </div>
    <div class="col-md-6 col-lg-4 projects__content">
      <a href="#">
        <img src="<?php echo theme_uri() ?>/files/img/projects/3.png" alt="">
      </a>
    </div>
    <div class="col-md-6 col-lg-4 projects__content">
      <a href="#">
        <img src="<?php echo theme_uri() ?>/files/img/projects/1.png" alt="">
      </a>
    </div>
    <div class="col-md-6 col-lg-4 projects__content">
      <a href="#">
        <img src="<?php echo theme_uri() ?>/files/img/projects/2.png" alt="">
      </a>
    </div>
    <div class="col-md-6 col-lg-4 projects__content">
      <a href="#">
        <img src="<?php echo theme_uri() ?>/files/img/projects/3.png" alt="">
      </a>
    </div>
  </div>
  </div>
</section>