<!-- Services section (4 across) -->

<section class="services">
  <div class="container-fluid no-padding">
    <div class="row">
      <?php 
      $rows = get_field('services_repeater', 'option');
      if( $rows ) {
      foreach( $rows as $row ) {

      $title = $row['service_name'];
      $excerpt = $row['service_excerpt'];
      $link = $row['service_link'];
      $BGColor = "background-color:" . $row['service_hex'];
      // $BGImage = "background-image:url(" . $row['service_image']['sizes']['960*423'] . ")";
      $BGImage = "background-image:url(" . $row['service_image']['url'] . ")";
      $textColor ="color:" . $row['service_text_colour']; 
      ?>

      <div class="col-md-6 p0">
      <?php if($link){ ?><a href='<?php echo $link['url']; ?>'><?php } ?>
        <div class="service" style="<?php echo $row['service_hex'] ? $BGColor.';' : ''; ?> 
                                    <?php echo $row['service_image'] ? $BGImage.';' : ''; ?>
                                    <?php echo $row['service_text_colour'] ? $textColor.';' : ''; ?> 
                                    ">
          <h4 class="service__heading" <?php $row['service_text_colour'] ? $textColor.';' : '';  ?>><?php echo $title; ?></h4>
          <p class="service__body" <?php $row['service_text_colour'] ? $textColor.';' : '';  ?>><?php echo $excerpt; ?></p>
        </div>
        </a>
      </div>
      <?php }
      }?>
    </div>
  </div>
</section>



