<!-- Full width text -->

<?php $rows = get_sub_field('full_width_text_group'); ?>

<section class="full-width-text <?php echo $rows['full_width_text_padding']; ?>">
  <div class="container-fluid">
    <div class="row">
      <div class="full-width-text__content">
        <?php echo $rows['full_width_text_text']; ?>
      </div>
    </div>
  </div>
</section>