<!-- Services section with icons -->

<section class="services-icons section--padding-large">
  <div class="container-fluid services-icons__container no-padding">
  <div class="services-icons__heading">
    <?php $head = get_field('services_icons', 'option')['services_icons_heading']; ?>
    <span><?php echo $head['services_icons_heading_number']; ?></span> 
    <span><?php echo $head['services_icons_heading_title']; ?></span>
    </div>
    <div class="row">
      <?php 
      $rows = get_field('services_icons', 'option')['services_icons_repeater'];
      if( $rows ) {
      foreach( $rows as $row ) {
      $title = $row['services_icons_name'];
      $excerpt = $row['services_icons_excerpt'];
      $link = $row['services_icons_link'];
      $icon = $row['services_icons_icon']; ?>

      <div class="col-sm-6 col-xl-3">
        <?php if($link){; ?>
        <a href='<?php echo $link['url']; ?>'> 
          <?php }?>
          <div class="services-icons__outer">
            <div class="services-icons__icon">
              <img src="<?php echo $icon['url']; ?>" alt="<?php echo($icon['alt'] ? $icon['alt'] : ''); ?>" class="services-icons__icon" loading='lazy'>
            </div>
            <div class="services-icons__content">
              <h4 class="services-icons__title"><?php echo $title; ?></h4>
              <?php echo $excerpt; ?>
            </div>
          </div>
        </div>
        <?php if($link){; ?>
        </a>
        <?php }
          }
        }?>
      </div>

    <div class="services-icons__buttons">
    <?php 
      $buttons = get_field('services_icons', 'option')['services_icons_buttons'];
      if( $buttons ) {
      foreach( $buttons as $button ) { ?>
        <a href="<?php echo $button['services_icons_button']['url']; ?>" class="button services-icons__button"><?php echo $button['services_icons_button']['title']; ?></a>
      <?php }
      }?>
      </div>
  </div>
</section>





