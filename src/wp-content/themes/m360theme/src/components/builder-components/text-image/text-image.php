<!-- Text to the left -->

<?php $rows = get_sub_field('text_left_group'); ?>

<section>
  <div class="container-fluid p0">
    <div class="text-image">
      <picture>
        <source srcset="<?php echo $rows['text_left_image']['sizes']['3840*1920'];?> 3840w, <?php echo $rows['text_left_image']['sizes']['1920*845'];?> 1920w" media="(min-width: <?php echo desktopBreakpoint() ?>)">
        <source srcset="<?php echo $rows['text_left_image']['sizes']['1920*845'];?>, <?php echo $rows['text_left_image']['sizes']['960*423'];?> 960w" media="(min-width: <?php echo tabletBreakpoint() ?>)">
        <img src="<?php echo $rows['text_left_image']['sizes']['750*1035-left'];?>" alt="" class="img-fluid">
      </picture>
      <div class="text-image__content">
        <h2 class="text-image__heading"><?php echo $rows['text_left_heading'];?></h2>
        <div class="text-image__body">
        <?php echo $rows['text_left_content'];?>
        </div>
      </div>
    </div>
  </div>
</section>