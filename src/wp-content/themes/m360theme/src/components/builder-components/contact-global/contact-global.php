<!-- Global Contact form -->

<?php $fields = get_field('global_fields_group', 'options');
$contact = get_field('contact_global', 'options');
$stockists = get_field('stockists', 'options'); 
?>

<section id="contact" class="contact section--padding section--padding-large">
  <div class="container-fluid">
    <div class="row">

      <div class="col-md-6">
        <div class="contact__upper">
          <h2 class="contact__heading"><?php echo($contact['contact_global_heading']);?></h2>
          <?php if(get_sub_field('contact_info'));{ ?>
          <div class="contact__info">
              <?php echo(get_sub_field('contact_info'));?>
          </div>
          <?php } ?>
          <?php echo do_shortcode( '[contact-form-7 id="64" title="Main Contact Form" html_class="contact-form"]' ); ?>
        </div>
        <div class="contact__content">

          <div class="row">
            <div class="col-sm-6">
              <div class="contact__top">
                <span>Telephone: <a href="<?php echo $fields['global_telephone']['url']; ?>" class="semibold"><?php echo $fields['global_telephone']['title']; ?></a></span>
                <span>Email: <a href="<?php echo $fields['global_email']['url']; ?>" class="semibold"><?php echo $fields['global_email']['title']; ?></a></span>
              </div>
              <div class="contact__mid semibold">
                <?php echo $fields['global_address']; ?>
              </div>
            </div>
            <div class="col-sm-6">
            </div>
          </div>
          </div>
      </div>

      <div class="col-md-6 offset-xl-1 col-xl-5 contact__outer">
        <h2 class="contact__heading">STOCKISTS</h2>
        <div class="acf-map contact__stockists-map" data-zoom="11">

        <?php 
        
        if($stockists){
        foreach($stockists as $loc){ 
          $name = $loc['stockist_title'];
          $address = $loc['stockist_address'];
          $phone = $loc['stockist_phone'];
          ?>

          <div class="marker" data-lat="<?php echo esc_attr($loc['stockist']['lat']); ?>" data-lng="<?php echo esc_attr($loc['stockist']['lng']); ?>">
            <?php if($name){ ?>
					  <p class="marker__name"><?php echo esc_attr($name); ?></p> 
            <?php }?>

            <?php if($address){ ?>
              <div class="marker__address"><?php echo ($address); ?></div> 
            <?php }?>

            <?php if($phone){ ?>
              <p class="marker__phone">Tel: <a href="tel:<?php echo esc_attr($phone); ?>"><?php echo esc_attr($phone); ?></a></p> 
			    	<?php }?>
          </div>

			<?php } ?>
			</div>
		</div>
		<?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>