//##########################################################################
// map.js
//##########################################################################

export const map = {
  elements: {
    markers: null,
    maps: null,
    customStyle: null,
    infoWindows: [],
  },

  state: {},

  init: function () {
    var self = this;
    self.elements.maps = $(".acf-map");
    self.elements.customStyle = [
      {
        featureType: "all",
        elementType: "labels.text.fill",
        stylers: [
          {
            saturation: 36,
          },
          {
            color: "#333333",
          },
          {
            lightness: 40,
          },
        ],
      },
      {
        featureType: "all",
        elementType: "labels.text.stroke",
        stylers: [
          {
            visibility: "on",
          },
          {
            color: "#ffffff",
          },
          {
            lightness: 16,
          },
        ],
      },
      {
        featureType: "all",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "administrative",
        elementType: "geometry.fill",
        stylers: [
          {
            color: "#fefefe",
          },
          {
            lightness: 20,
          },
        ],
      },
      {
        featureType: "administrative",
        elementType: "geometry.stroke",
        stylers: [
          {
            color: "#809c90",
          },
          {
            lightness: 17,
          },
          {
            weight: 1.2,
          },
        ],
      },
      {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#658074",
          },
        ],
      },
      {
        featureType: "administrative",
        elementType: "labels.text.stroke",
        stylers: [
          {
            color: "#ffffff",
          },
        ],
      },
      {
        featureType: "administrative.country",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#658074",
          },
        ],
      },
      {
        featureType: "administrative.province",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#658074",
          },
        ],
      },
      {
        featureType: "administrative.locality",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#658074",
          },
        ],
      },
      {
        featureType: "administrative.neighborhood",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#658074",
          },
        ],
      },
      {
        featureType: "administrative.land_parcel",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#658074",
          },
        ],
      },
      {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [
          {
            color: "#8bbaa6",
          },
          {
            lightness: 20,
          },
        ],
      },
      {
        featureType: "poi",
        elementType: "geometry",
        stylers: [
          {
            color: "#f5f5f5",
          },
          {
            lightness: 21,
          },
        ],
      },
      {
        featureType: "poi.park",
        elementType: "geometry",
        stylers: [
          {
            color: "#75a490",
          },
          {
            lightness: 21,
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [
          {
            color: "#809c90",
          },
          {
            lightness: 17,
          },
        ],
      },
      {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [
          {
            color: "#b5dccb",
          },
          {
            lightness: 29,
          },
          {
            weight: 0.2,
          },
        ],
      },
      {
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [
          {
            color: "#ffffff",
          },
          {
            lightness: 18,
          },
        ],
      },
      {
        featureType: "road.local",
        elementType: "geometry",
        stylers: [
          {
            color: "#ffffff",
          },
          {
            lightness: 16,
          },
        ],
      },
      {
        featureType: "transit",
        elementType: "geometry",
        stylers: [
          {
            color: "#f2f2f2",
          },
          {
            lightness: 19,
          },
        ],
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [
          {
            color: "#c9e3d8",
          },
          {
            lightness: 17,
          },
        ],
      },
    ];
    function initMap($el) {
      // Find marker elements within map.
      self.elements.markers = $el.find(".marker");

      var mapArgs = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        zoom: 8,
      };
      var map = new google.maps.Map($el[0], mapArgs);

      // Add markers.
      map.markers = [];
      self.elements.markers.each(function () {
        self.initMarker($(this), map);
      });

      map.setOptions({ styles: self.elements.customStyle });

      // Center map based on markers.
      self.centerMap(map);

      // Return map instance.
      return map;
    }

    // Render maps on page load.
    $(document).ready(function () {
      self.elements.maps.each(function () {
        var map = initMap($(this));
      });
    });
  },

  initMarker: function ($marker, map) {
    var self = this;
    // Get position from marker.
    var lat = $marker.data("lat");
    var lng = $marker.data("lng");
    var latLng = {
      lat: parseFloat(lat),
      lng: parseFloat(lng),
    };

    // Create marker instance.
    var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      icon: stylesheet_directory_uri + "/files/img/icons/custom-marker.svg",
    });

    // Append to reference for later use.
    map.markers.push(marker);

    // If marker contains HTML, add it to an infoWindow.
    if ($marker.html()) {
      console.log("here");
      // Create info window.
      var infoWindow = new google.maps.InfoWindow({
        content: $marker.html(),
      });

      self.elements.infoWindows.push(infoWindow);

      // Show info window when marker is clicked.
      google.maps.event.addListener(marker, "click", function () {
        //close all
        for (var i = 0; i < self.elements.infoWindows.length; i++) {
          self.elements.infoWindows[i].close();
        }
        infoWindow.open(map, marker);
      });

      google.maps.event.addListener(map, "click", function () {
        infoWindow.close();
      });
    }
  },
  centerMap: function (map) {
    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function (marker) {
      bounds.extend({
        lat: marker.position.lat(),
        lng: marker.position.lng(),
      });
    });

    // Case: Single marker.
    // if (map.markers.length == 1) {
    //   map.setCenter(bounds.getCenter());

    //   // Case: Multiple markers.
    // } else {
    //   map.fitBounds(bounds);
    // }
    map.setCenter(bounds.getCenter());
  },
};

document.addEventListener("DOMContentLoaded", () => {
  map.init();
});
