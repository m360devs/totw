<!-- Products-->
<?php 
$fields = get_sub_field('pet_food_fields');
$blocks = get_sub_field('pet_food_content_blocks');
$padding = get_sub_field('pet_food_padding');
?>

<!-- Pet food-->
<section class="pet-food <?php echo $padding; ?>">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-5 pet-food__left">
        <img src="<?php echo $fields['pet_food_image']['url']; ?>" class="pet-food__packet" alt="<?php echo $fields['pet_food_image']['alt'] ? $fields['pet_food_image']['alt'] : ''; ?>">
        <img src="<?php echo $fields['pet_food_graphic']['url']; ?>" class="pet-food__graphic" alt="<?php echo $fields['pet_food_graphic']['alt'] ? $fields['pet_food_graphic']['alt'] : ''; ?>">
      </div>
      <div class="col-md-7 pet-food_right">
        <div class="pet-food__content">
          <h1 class="pet-food__heading"><?php echo $fields['pet_food_title']; ?></h1>
          <h2 class="pet-food__main-sub-heading"><?php echo $fields['pet_food_sub_title']; ?></h2>
          <?php if($blocks){
          foreach($blocks as $block){
            ?>
            <h2 class="pet-food__sub-heading"><?php echo $block['pet_food_sub_heading'] ?></h2>

              <?php echo $block['pet_food_content'] ?>
            
            <?php }
              }
            ?>
        </div>
      </div>
    </div>

  </div>
</section>





