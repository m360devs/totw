<!-- Hero carousel -->

<?php $slides = get_sub_field('hero_slides'); ?>

<section class="hero">
  <div class="container-fluid">
    <div class="row">
      <div class="hero__carousel owl-carousel owl-theme">

      <?php 
      if(is_array($slides)){
      foreach($slides as $slide){ ?>
        <div class="hero__slide">
          <?php if (get_sub_field('hero_tint')){?>
          <span class="hero__tint" style="background-color: <?php echo get_sub_field('hero_tint');?>"></span>
          <?php }?>
          <picture class="hero__image" alt="<?php echo ($slide['slide_image']['alt'] ? $slide['slide_image']['alt'] :'')?>">
            <source srcset="<?php echo $slide['slide_image']['sizes']['3840*1920']?> 3840w, <?php echo $slide['slide_image']['sizes']['1920*845']?> 1920w" media="(min-width: <?php echo desktopBreakpoint() ?>)">
            <source srcset="<?php echo $slide['slide_image']['sizes']['1920*845']?> 1920w, <?php echo $slide['slide_image']['sizes']['960*423']?> 960w" media="(min-width: <?php echo tabletBreakpoint() ?>)">
            <img src="<?php echo $slide['slide_image']['sizes']['750*680']?>" alt="<?php echo $slide['slide_image']['alt']?>" class="img-fluid">
          </picture>

          <div class="hero__slide-content-top">
            <p class="hero__heading"><?php echo $slide['slide_title']; ?></p>
            <p class="hero__sub-heading"><?php echo $slide['slide_subtext']; ?></p>
          </div>
          <div class="hero__slide-content-bottom">
          <?php $rows = $slide['slide_buttons'];
          foreach($rows as $row){
          ?>
            <a href="<?php echo $row['slide_button']['url']; ?>" class="button hero__button"><?php echo $row['slide_button']['title']; ?></a>
          <?php }?>
          </div>
        </div>
        
        <?php }
         }?>
      </div>
    </div>
  </div>
</section>