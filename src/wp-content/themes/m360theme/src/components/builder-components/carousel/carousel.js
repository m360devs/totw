//##########################################################################
// header.js
//##########################################################################

export const header = {
  elements: {
    header: null,
  },

  state: {
    menuOpen: false,
  },

  init: function () {
    var self = this;
    $(document).ready(function () {
      $(".hero__carousel").owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        items: 1,
      });
    });
  },
};

document.addEventListener("DOMContentLoaded", () => {
  header.init();
});
