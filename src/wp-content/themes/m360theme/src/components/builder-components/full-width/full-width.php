<!-- Full width -->

<?php 
$padding = get_sub_field('full_width_group')['full_width_padding'];
$rows = get_sub_field('full_width_group')['full_width_repeater']; 
?>

<section class="full-width <?php if($padding) echo $padding;?>">
  <div class="container-fluid">

  <?php if( $rows ) {
  foreach( $rows as $row ) { ?>

    <div class="row">
      <div class="full-width__content">
        <?php if($row['full_width_image']){?> <img src="<?php echo $row['full_width_image']['url']; ?>" class="full-width__image" alt="<?php echo ($row['full_width_image']['alt'] ? $row['full_width_image']['alt'] : '') ?> loading='lazy' "><?php } ?>
        <h2 class="full-width__heading"><?php if($row['full_width_heading']) echo $row['full_width_heading']; ?></h2>
        <p><?php if($row['full_width_text']) echo $row['full_width_text']; ?></p>
      </div>
    </div>

    <?php }       
    }?>

  </div>
</section>