<!-- Full width image -->

<?php $rows = get_sub_field('full_width_image_image'); ?>

<section class="full-width-image">
  <picture alt="<?php echo $rows['url'] ? $rows['url'] : ''?>">
    <source srcset="<?php echo $rows['sizes']['3840*1920'];?> 3840w, <?php echo $rows['sizes']['1920*845'];?> 1920w" media="(min-width: <?php echo desktopBreakpoint() ?>)">
    <source srcset="<?php echo $rows['sizes']['1920*845'];?>, <?php echo $rows['sizes']['960*423'];?> 960w" media="(min-width: <?php echo tabletBreakpoint() ?>)">
    <img src="<?php echo $rows['sizes']['750*680'];?>" alt="" class="img-fluid">
  </picture>
</section>

