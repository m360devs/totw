<!-- Footer -->

<?php $social = get_field('social_fields', 'options'); ?>
<?php $contact = get_field('global_fields_group', 'options'); ?>
<footer class="footer section--padding-large">
  <div class="page-container">

    <div class="row">
      <div class="col-lg-4 footer__left">
        <img src="<?php echo theme_uri() ?>/files/img/logo.svg" class="footer__logo" alt="logo">
        <div class="footer__links">
          <p>Telephone: <a href="<?php echo $contact['global_telephone']['url']; ?>"><?php echo $contact['global_telephone']['title']; ?></a></p>
          <p>Email: <a href="<?php echo $contact['global_email']['url']; ?>"><?php echo $contact['global_email']['title']; ?></a></p>
        </div>
      </div>
      <div class="col-lg-2 offset-lg-2 footer__center">
        <?php
        wp_nav_menu(array(
          'theme_location' => 'secondary',
          'menu_class' => 'footer__services-menu',
          'container' => false
        ));
      ?>
      </div>
      <div class="col-lg-2 col-xl-2 footer__center">
        <ul class="footer__services-menu">
          <li>
            <a href="/#contact">Stockists</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-2 footer__right">
      <?php if ($social) { ?><span class="footer__heading">Follow us</span>
        <div class="social-icon__group">
          <?php foreach($social as $row){ ?>
           
           <a href="<?php echo $row['social_link']['url']; ?>"> 
             <span class="social-icon social-icon--twitter icon--light"><?php get_template_part('/files/icons/svgs/'.$row['social_platform'] ); ?></span>
          </a>
          <?php } ?>
        </div>
        <?php }?>
      </div>
    </div>

  </div>
</footer>
