<!-- Header -->
<?php $contact = get_field('global_fields_group', 'options'); ?>

<header class="header">
<div class="page-container">
  <div class="row">
    <div class="header__content">
      <div class='header__left'>
        <div class="header__logo-container">
          <h1>
            <a href="<?php echo home_url(); ?>"><img src="<?php echo theme_uri() ?>/files/img/logo.svg" class="header__logo" alt="Simply made for the best nutrition"></a>
          </h1>
          <div class="header__usa">
            <img src="<?php echo theme_uri() ?>/files/img/usa.svg" class="header__usa-image" alt="USA flag">
            <p class="header__usa-tagline">MADE IN THE USA</p>
          </div>
        </div>
      </div>
      <div class="header__right">
      <div class="header__menu-container">
      <a href="<?php echo home_url(); ?>"><img src="<?php echo theme_uri() ?>/files/img/logo.svg" class="header__menu-mobile-logo" alt="logo"></a>
        <div class="header__usa-mobile">
          <img src="<?php echo theme_uri() ?>/files/img/usa.svg" class="header__usa-image-mobile" alt="USA flag">
          <p class="header__usa-tagline-mobile">MADE IN THE USA</p>
        </div>

        <?php
          wp_nav_menu(array(
            'theme_location' => 'primary',
            'menu_class' => 'header__menu',
            'container' => false
          ));
        ?>
    </div>

    </div>
    <button class="header__menu-btn" aria-expanded="false" aria-controls="header-menu">
        <span class="sr-only">Open/Close Menu</span>
        <span class="header__menu-btn__icon" aria-hidden="true" focusable="false" tabindex="-1">
          <span class="header__menu-btn__icon--middle-anti-clock"></span>
          <span class="header__menu-btn__icon--middle-clock"></span>
        </span>
      </button>
  </div>
</div>
</header>




