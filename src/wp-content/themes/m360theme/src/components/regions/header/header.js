//##########################################################################
// header.js
//##########################################################################

export const header = {
  elements: {
    header: null,
    subMenus: null,
    subMenuLinks: null,
  },

  state: {
    isBelowBreakpoint: false,
    menuOpen: false,
    isMobile: null,
  },

  init() {
    const self = this;

    // Local elements
    //
    self.elements.header = document.querySelector(".header");
    self.elements.headerMenuBtn = self.elements.header.querySelector(
      ".header__menu-btn"
    );
    self.elements.subMenus = document.querySelectorAll(
      ".menu-item-has-children"
    );
    self.elements.subMenuLinks = document.querySelectorAll(
      ".header__content .menu-item-has-children > a"
    );

    // Handle menu and resize
    //
    self.handleMenu();
    self.handleResize();
    self.handleMenuHeight();

    // Set initial state isMobile
    //
    if (window.innerWidth >= window.app.breakpoints.desktop) {
      self.state.isMobile = false;
    } else if (window.innerWidth < window.app.breakpoints.desktop) {
      self.state.isMobile = true;
    }
  },

  handleMenu() {
    const self = this;

    // On click: menu button to toggle the menu open/closed
    //
    self.elements.headerMenuBtn.addEventListener("click", (e) => {
      e.preventDefault();

      if (self.state.menuOpen === false) {
        self.openMenu();
        self.addSubMenuLinks();
      } else if (self.state.menuOpen === true) {
        self.closeMenu();
        self.removeSubMenuLinks();
      }
    });
  },

  handleMenuHeight() {
    const self = this;
    var sections = document.getElementsByTagName("section");
    var h = window
      .getComputedStyle(self.elements.header, null)
      .getPropertyValue("height");
    sections[0].style.marginTop = h;
  },

  addSubMenuLinks() {
    const self = this;

    // Create buttons for links with sub menus, attach btn listener and add to DOM
    //

    self.elements.subMenuLinks.forEach(function (item) {
      var newNode = document.createElement("img");
      newNode.className = "header__sub-menu-toggle";
      newNode.src =
        stylesheet_directory_uri + "/files/img/icons/chevron-down.svg";
      newNode.addEventListener("click", (e) => {
        e.preventDefault();
        self.openSubMenu(item);
      });
      item.append(newNode);
    });
  },

  removeSubMenuLinks() {
    // Remove buttons for links with sub menus, attach btn listener and add to DOM
    //

    document
      .querySelectorAll(".header__sub-menu-toggle")
      .forEach((e) => e.remove());
  },

  openSubMenu: function (item) {
    const self = this;

    item.nextElementSibling.classList.toggle("is-open");
  },

  openMenu() {
    const self = this;

    if (self.state.menuOpen === false) {
      self.elements.header.classList.add("is-active");
      window.app.html.classList.add("no-scroll");
      self.state.menuOpen = true;
    }
  },

  closeMenu() {
    const self = this;

    if (self.state.menuOpen === true) {
      self.elements.header.classList.remove("is-active");
      window.app.html.classList.remove("no-scroll");
      self.state.menuOpen = false;
    }
  },

  handleResize() {
    const self = this;

    // On resize: if going from mobile state to desktop state
    // and if the menu is open, then close it
    //
    window.addEventListener(
      "resize",
      window.utils.throttle(() => {
        if (window.innerWidth >= window.app.breakpoints.desktop) {
          if (self.state.isMobile === true) {
            self.state.isMobile = false;
            self.removeSubMenuLinks();
            self.closeMenu();
          }
        } else {
          if (self.state.isMobile === false) {
            self.state.isMobile = true;
            self.addSubMenuLinks();
          }
        }
        self.handleMenuHeight();
      }, 250)
    );
  },
};

document.addEventListener("DOMContentLoaded", () => {
  header.init();
});
