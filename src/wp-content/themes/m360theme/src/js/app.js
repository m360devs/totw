//###########################################################################
// App
//###########################################################################

/***********************
  App is hooked onto the window object within window.app, this is so app properties
  can be globally accessed without the need for importing
************************/

window.app = {
  states: {
    loading: "is--loading",
    active: "is--active",
    expanded: "is--expanded",
    noScroll: "no--scroll",
    hasMenu: "has--menu",
  },

  doc: null,
  body: null,
  page: null,

  // These should reflect css breakpoints to avoid confusion
  // unless otherwise required
  //
  breakpoints: {
    mobile: 360,
    mobileLandscape: 480,
    tablet: 768,
    desktop: 992,
    wide: 1280,
    extraWide: 1440,
  },
};

// Define window.utils for utilities
//------------------

window.utils = {};

// On DOMContentLoaded set values
//------------------

document.addEventListener("DOMContentLoaded", () => {
  window.app.doc = document.documentElement;
  window.app.body = document.getElementsByTagName("body")[0];
  window.app.html = document.getElementsByTagName("html")[0];
  window.app.page = document.querySelector(".page");

  // Remove .no-js from html element, replace with .js
  //------------------
  window.app.doc.classList.remove("no-js");
  window.app.doc.classList.add("js");
});

// var maxHeight = 0;

// if ($(".grow")) {
//   $(".grow").each(function () {
//     if ($(this).height() > maxHeight) {
//       maxHeight = $(this).height();
//     }
//   });

//   $(".grow").height(maxHeight);
// }
