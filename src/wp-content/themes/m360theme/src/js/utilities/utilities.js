//###########################################################################
// Utilities
//###########################################################################

/***********************
  Utilities are hooked into the window object within window.utils, this is so utility
  functions are available globally without having to import functions that are commonly
  used, any new utilities be sure to format as window.utils.newUtil = () => {...
************************/

import "./_http.js"
import "./_throttle-debounce.js"
