//###########################################################################
// Http
//###########################################################################

window.utils.http = (options) => {
  if (options.before) {
    options.before()
  }

  let request = new XMLHttpRequest()
  request.open(options.method ? options.method : "GET", options.url, true)
  request.setRequestHeader("x-requested-with", "XMLHttpRequest")

  if (options.method && options.method.toLowerCase() == "post") {
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  }

  if (options.headers !== null) {
      for (let key in options.headers) {
        let value = options.headers[key]
        request.setRequestHeader(key, value)
      }
  }

  request.send(options.data)

  if (options.onload) {
    request.onload = function() {
      options.onload(request.response);
    }
  }

  request.onerror = function() {
    log('There was an error trying to fetch the data.')
  }
}

// Use examples:
//
/*

window.utils.http({
  method: 'GET',
  url: 'https://reqres.in/api/products/3',
  headers: {
    'Access-Control-Allow-Origin': '*'
  },
  onload: (data) => {
    console.log(JSON.parse(data));
  },
  before: () => {
    console.log('before request');
  },
})

window.utils.http({
  method: 'POST',
  url: 'https://reqres.in/api/users',
  headers: {
    'Access-Control-Allow-Origin': '*'
  },
  data: {
    name: "paul rudd",
    movies: ["I Love You Man", "Role Models"]
  },
  onload: (data) => {
    console.log(JSON.parse(data));
  },
  before: () => {
    console.log('before request');
  },
})

*/
