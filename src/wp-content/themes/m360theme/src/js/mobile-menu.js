//##########################################################################
// Header
//##########################################################################

class MobileMenu {
  render() {
    var app = {
      body: null,
      header: null,
      headerHamburger: null,
      mobileMenu: null,

      state: {
        menuOpen: false,
      },

      init: function () {
        var self = this;

        // Set local elements
        self.body = $("body");
        self.header = $(".header__menu");
        self.headerHamburger = $(".header__menu-btn");
        self.headerSubMenus = $(".header__menu .menu-item-has-children");

        var menuHeight = self.header.outerHeight();

        // Add click listener
        self.handleMenu();
        self.handleSubMenus();
      },

      handleMenu: function () {
        var self = this;

        self.headerHamburger.click(function () {
          if (self.state.menuOpen === false) {
            self.openMenu();
          } else if (self.state.menuOpen === true) {
            self.closeMenu();
          }
        });
      },

      handleSubMenus: function () {
        var self = this;

        self.headerSubMenus.each(function (index, sub) {
          sub.addEventListener("click", function (e) {
            var self = this;
            $(sub).toggleClass("open-sub-menu");
          });
        });
      },

      openMenu: function () {
        var self = this;

        if (self.state.menuOpen === false) {
          // Open menu
          self.body.addClass("is--open");

          // Set menu button aria state
          self.headerHamburger.attr("aria-expanded", true);

          self.state.menuOpen = true;
        }
      },

      closeMenu: function () {
        var self = this;

        if (self.state.menuOpen === true) {
          // Close menu
          self.body.removeClass("is--open");

          // Set menu button aria state
          self.headerHamburger.attr("aria-expanded", false);

          // Set state of menu
          self.state.menuOpen = false;
        }
      },
    };

    $(document).ready(function () {
      app.init();
    });
  }
}

export default MobileMenu;
