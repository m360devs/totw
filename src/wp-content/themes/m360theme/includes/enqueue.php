<?php
  //##########################################################################
  // Enqueue
  //##########################################################################
function m360_enqueue(){
  $uri = get_theme_file_uri();
  $ver = M360_DEV_MODE ? time() : false;

  wp_register_style( 'm360_style', $uri . '/dist/styles.css', [], $ver);
  wp_register_script( 'm360_functions', $uri . '/dist/bundle.js', [], $ver, true );
  wp_enqueue_script( 'google_js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBheHcrBzCKENQO8n1mOWjUWsvmDsrjVFQ', [], $ver, true  );
  
  wp_enqueue_style( 'm360_style' );

  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'm360_functions' );
}