<?php
//##########################################################################
// Contact form 7 
//##########################################################################

// Disable auto <p> tags around form elements
//
function cf7Config(){
  add_filter('wpcf7_autop_or_not', '__return_false');
}