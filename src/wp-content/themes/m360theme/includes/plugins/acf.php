<?php 

//##########################################################################
// ACF
//##########################################################################

// Set up options pages
//
function acfConfig(){
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
      'page_title' 	=> 'Theme General Settings',
      'menu_title'	=> 'Theme Settings',
      'menu_slug' 	=> 'theme-general-settings',
      'capability'	=> 'edit_posts',
      'redirect'		=> false
    ));
    
    acf_add_options_sub_page(array(
      'page_title' 	=> 'Global Settings',
      'menu_title'	=> 'Global Settings',
      'parent_slug'	=> 'theme-general-settings',
    ));
    
    acf_add_options_sub_page(array(
      'page_title' 	=> 'Global Sections',
      'menu_title'	=> 'Global Sections',
      'parent_slug'	=> 'theme-general-settings',
    ));
  }

// Set up google maps
//
function my_acf_google_map_api( $api ){
  $api['key'] = 'AIzaSyBheHcrBzCKENQO8n1mOWjUWsvmDsrjVFQ';
  return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
}