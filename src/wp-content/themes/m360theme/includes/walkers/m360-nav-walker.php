<?php

//##########################################################################
// M360 Nav Walker
//##########################################################################

class M360_Nav_Walker extends Walker_Nav_Menu {

  function start_lvl(&$output, $depth = 0, $args = NULL) {
    // Depth dependent classes
    //
    $indent = ($depth > 0  ? str_repeat( "\t", $depth ) : '');
    $display_depth = ($depth + 1); // because it counts the first submenu as 0
    $classes = array(
      'header__submenu',
      'depth__' . $display_depth
    );
    $class_names = implode( ' ', $classes );

    // If submenu
    //
    if ($display_depth == 1) {
      $output .= "\n" . $indent . '<div class="header__submenu__wrapper"><ul class="' . $class_names . '">' . "\n";
    }
    // If default menu
    else {
      $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }
  }

  function end_lvl(&$output, $depth = 0, $args = NULL) {
      // $indent = str_repeat("\t", $depth);
      // $output .= "$indent</div>\n";
  }

  function start_el(&$output, $item, $depth = 0, $args = NULL, $id = 0) {
    global $wp_query;

    $class_names = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    // If default menu item
    //
    if ($depth == 0) {
      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
      $class_names = 'header__menu__item ' . esc_attr( $class_names );
      $output .= '<li class="' . $class_names . '" id="header__menu__item--' . $item->ID . '">';
    }
    // Else if submenu item
    //
    else {
      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
      $class_names = 'header__submenu__item ' . esc_attr( $class_names );
      $output .= '<li class="' . $class_names . '" id="header__submenu__item--' . $item->ID . '">';
    }

    $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
    $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
    $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
    $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

    //$item_output = $args['before'];
    $item_output = '<a'. $attributes .'>';
    $item_output .= apply_filters( 'the_title', $item->title, $item->ID );
    $item_output .= '</a>';
    //$item_output .= $args['after'];

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el(&$output, $item, $depth = 0, $args = NULL) {

  }
}

?>
