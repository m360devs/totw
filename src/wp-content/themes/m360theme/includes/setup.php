<?php

function m360_setup_theme(){
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'title-tag' );


  //##########################################################################
  // Images
  //##########################################################################

  add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );
  // Remove default image sizes here. 
  //
  function prefix_remove_default_images( $sizes ) {
  unset( $sizes['small']); // 150px
  unset( $sizes['medium']); // 300px
  unset( $sizes['large']); // 1024px
  unset( $sizes['medium_large']); // 768px
  return $sizes;
  }

  // Set custom image sizes
  //
  add_image_size('3840*1920', 3840, 1920, true);
  add_image_size('1920*845', 1920, 845, true);
  add_image_size( '960*423', 960, 423, true );
  add_image_size( '750*680', 750, 680, true );
  add_image_size( '1054*642', 1054, 642, true );
  add_image_size( '527*321', 527, 321, true );
  add_image_size( '352*216', 352, 216, true );
  add_image_size( '750*1035-left', 750, 1035, array( 'left', 'center' ) );

  //##########################################################################
  // General
  //##########################################################################

  // Get theme_uri
  //
  function theme_uri() {
    return get_theme_file_uri();
  }

  // Include file with variables
  //------------------

  function includeFile($fileName, $variables = null) {
    if ( isset($variables) ) {
      extract($variables);
    }

    include($fileName);
  }

  // Add page slug as body class
  //------------------

  function addSlugBodyClass($classes) {
    global $post;
    if ( isset($post) ) {
      if ( !is_front_page() ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
      }
    }
    return $classes;
  }
  add_filter( 'body_class', 'addSlugBodyClass' );

  //##########################################################################
  // Gutenberg
  //##########################################################################

  // Disable for posts
  //
  add_filter('use_block_editor_for_post', '__return_false', 10);

  // Disable for post types
  //
  add_filter('use_block_editor_for_post_type', '__return_false', 10);

  //##########################################################################
  // Register menus
  //##########################################################################

  register_nav_menu( 'primary', __('Primary Menu', 'm360') );
  register_nav_menu( 'secondary', __('Secondary Menu', 'm360') );

  //##########################################################################
  // Walkers
  //##########################################################################

  include( get_theme_file_path('/includes/walkers/m360-nav-walker.php') );
}

//##########################################################################
// Hide default editor
//##########################################################################

// Across all pages
//
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  remove_post_type_support('page', 'editor');
}

// Across specific pages
//
// add_action( 'admin_init', 'hide_editor' );
// function hide_editor() {
//   $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
//   if( !isset( $post_id ) ) return;
//   $about = get_the_title($post_id);
//   $contact = get_the_title($post_id);
//   if($about == 'About' || $contact == 'Contact'){ 
//     remove_post_type_support('page', 'editor');
//   }
// }

//##########################################################################
// Image breakpoints
//##########################################################################

function mobileLandscapeBreakpoint() {
  return "479px";
}

function tabletBreakpoint() {
  return "767px";
}

function desktopBreakpoint() {
  return "991px";
}

function wideBreakpoint() {
  return "1199px";
}