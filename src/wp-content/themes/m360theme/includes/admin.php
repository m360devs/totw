<?php
//##########################################################################
// Login setup
//##########################################################################
function my_login_logo() { ?>
  <style>
  .login {
    background: #485C6D;
  }
  .login #wp-submit:hover {
    background: #c30044 !important;
  }
  .login #wp-submit {
    background: #e40050 !important;
    border: none !important;
  }
  #login h1 a, .login h1 a {
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/m360.svg);
    height: 52px;
    width: 231px;
    background-size: 100% 100%;
    background-repeat: no-repeat;
    padding-bottom: 30px;
  }
  #nav a:hover, #backtoblog a:hover {
    color: #e40050 !important;
  }
  #nav a, #backtoblog a {
    color: #ffffff !important;
  }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
?>