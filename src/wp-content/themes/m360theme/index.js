//###########################################################################
// Scripts
//###########################################################################

// App
//------------------

import "./src/js/app.js";

// Polyfills
//------------------

import "./src/js/polyfills/polyfills.js";

// Utilities
//------------------

import "./src/js/utilities/utilities.js";

// Libraries
//------------------

// import "jQuery";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel";

// Components
//------------------

import "./src/components/components.js";

// styles
//------------------

import "./src/scss/style.scss";

// import "bootstrap/dist/css/bootstrap.min.css";

// Environment
//------------------

if (process.env.NODE_ENV == "production") {
  console.log("Production");
} else if (process.env.NODE_ENV == "development") {
  console.log("Development");
} else {
  console.log("No environment");
}
